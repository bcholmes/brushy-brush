//
//  Document.m
//  Brushy Brush
//
//  Created by BC Holmes on 2018-01-08.
//  Copyright © 2018 Ayizan Studios. All rights reserved.
//

#import "BCHBrushDocument.h"

#import "AbrHeader.h"
#import "BCHBrush.h"
#import "BCHDataStream.h"
#import "BCHSegment.h"
#import "BCHSampSegment.h"

@interface BCHBrushDocument ()

@property (nonatomic, strong) NSArray* brushList;

@end

typedef struct AbrInfoHeader {
    short version;
    short subversion;
    short count;
} AbrInfoHeader;


@implementation BCHBrushDocument

- (instancetype)init {
    self = [super init];
    if (self) {
        // Add your subclass-specific initialization here.
    }
    return self;
}

+ (BOOL)autosavesInPlace {
    return NO;
}


- (void)makeWindowControllers {
    // Override to return the Storyboard file name of the document.
    [self addWindowController:[[NSStoryboard storyboardWithName:@"Main" bundle:nil] instantiateControllerWithIdentifier:@"Document Window Controller"]];
}


- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError {
    // Insert code here to write your document to data of the specified type. If outError != NULL, ensure that you create and set an appropriate error when returning nil.
    // You can also choose to override -fileWrapperOfType:error:, -writeToURL:ofType:error:, or -writeToURL:ofType:forSaveOperation:originalContentsURL:error: instead.
    [NSException raise:@"UnimplementedMethod" format:@"%@ is unimplemented", NSStringFromSelector(_cmd)];
    return nil;
}


- (BOOL) readFromData:(NSData*) data ofType:(NSString*) typeName error:(NSError**) outError {

    self.header = [AbrHeader new];
    BCHDataStream* stream = [[BCHDataStream alloc] initWithData:data];
    
    self.header.version = [stream readBigEndianUInt16];
    if (self.header.version == 6) {
        self.header.subversion = [stream readBigEndianUInt16];
    } else {
        self.header.count = [stream readBigEndianUInt16];
    }
    
    NSLog(@"Version number : %d", (int) self.header.version);
    NSLog(@"Sub-Version number : %d", (int) self.header.subversion);
    NSLog(@"Count : %d", (int) self.header.count);

    if (self.header.version == 6) {
        [self loadVersion6Brushes:stream];
    }
//    [NSException raise:@"UnimplementedMethod" format:@"%@ is unimplemented", NSStringFromSelector(_cmd)];
    return YES;
}


- (BCHSegment*) createSegmentOfAppropriateType:(NSString*)tagName {
    if ([tagName isEqualToString:@"samp"]) {
        BCHSampSegment* temp = [BCHSampSegment new];
        temp.subVersion = self.header.subversion;
        temp.tagName = tagName;
        return temp;
    } else {
        BCHSegment* segment = [BCHSegment new];
        segment.tagName = tagName;
        return segment;
    }
}

-(void) loadVersion6Brushes:(BCHDataStream*) dataStream {
    NSData* data = [dataStream readDataOfLength:4];
    
    NSMutableDictionary* segments = [NSMutableDictionary new];
    
    while (data != nil) {
        NSString* tagName = [[NSString alloc] initWithBytes:[[dataStream readDataOfLength:4] bytes] length:4 encoding:NSASCIIStringEncoding];
        BCHSegment * segment = [self createSegmentOfAppropriateType:tagName];
        segment.tagName = tagName;
        uint32_t length = [dataStream readBigEndianUInt32];
        
        NSLog(@"tag: >%@<, data length: %u", segment.tagName, length);
        
        segment.data = [dataStream readDataOfLength:length];
        data = [dataStream readDataOfLength:4];
        [segments setObject:segment forKey:segment.tagName];
        /*
        if ([tagName isEqualToString:@"desc"]) {
            [self hexDump:segment.data];
        }
         */
    }
    
    self.segments = [NSDictionary dictionaryWithDictionary:segments];
}

-(void) hexDump:(NSData*) data {
    
    unsigned char* bytes = (unsigned char*) [data bytes];
    
    for (int i = 0; i < (data.length + 19) / 20; i++) {
        NSMutableString* hex = [NSMutableString new];
        NSMutableString* string = [NSMutableString new];

        for (int j = 0; j < 20 && (i * 20 + j) < data.length; j++) {
            
            if (j == 10) {
                [hex appendString:@" "];
                [string appendString:@" "];
            }
            [hex appendFormat:@"%02x ", bytes[i*20 + j]];
            
            unsigned char c = bytes[i*20+j];
            if (c >=  32 && c <= 126) {
                [string appendString:[NSString stringWithFormat:@"%c ", c]];
            } else {
                [string appendString:@". "];
            }
        }
        
        NSLog(@"> %@  %@", hex, string);
    }
}

-(NSArray*) brushes {
    if (self.brushList == nil) {
        BCHSampSegment* segment = [self.segments objectForKey:@"samp"];
        NSDictionary* samples = [segment samples];

        NSMutableArray* array = [NSMutableArray new];
        for (NSString* key in [samples allKeys]) {
            BCHBrush* brush = [BCHBrush new];
            brush.brushTipImageKey = key;
            brush.brushTipImage = [samples objectForKey:key];
            [array addObject:brush];
        }
        self.brushList = [NSArray arrayWithArray:array];
    }
    return self.brushList;
}


@end
