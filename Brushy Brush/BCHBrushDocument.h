//
//  Document.h
//  Brushy Brush
//
//  Created by BC Holmes on 2018-01-08.
//  Copyright © 2018 Ayizan Studios. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "AbrHeader.h"

@interface BCHBrushDocument : NSDocument

@property (nonnull, nonatomic, strong) AbrHeader* header;
@property (nonnull, nonatomic, strong) NSDictionary* segments;

@property (nonnull, nonatomic, readonly) NSArray* brushes;

@end

