//
//  BCHSampSegment.h
//  Brushy Brush
//
//  Created by BC Holmes on 2018-08-22.
//  Copyright © 2018 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BCHSegment.h"

@interface BCHSampSegment : BCHSegment

@property (nonatomic, assign) short subVersion;
@property (nonatomic, readonly) NSDictionary* samples;

@end
