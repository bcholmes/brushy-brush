//
//  AbrHeader.h
//  Brushy Brush
//
//  Created by BC Holmes on 2018-01-13.
//  Copyright © 2018 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AbrHeader : NSObject

@property (assign, nonatomic) short version;
@property (assign, nonatomic) short subversion;
@property (assign, nonatomic) short count;

@property (nonatomic, readonly) NSString* fullVersion;

@end
