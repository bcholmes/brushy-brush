//
//  AbrHeader.m
//  Brushy Brush
//
//  Created by BC Holmes on 2018-01-13.
//  Copyright © 2018 Ayizan Studios. All rights reserved.
//

#import "AbrHeader.h"

@implementation AbrHeader

-(NSString*) fullVersion {
    return (self.version == 1 || self.version == 2) ? [[NSNumber numberWithInt:(int) self.version] stringValue] : [NSString stringWithFormat:@"%d.%d", (int) self.version, (int) self.subversion];
}

@end
