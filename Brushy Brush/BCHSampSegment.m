//
//  BCHSampSegment.m
//  Brushy Brush
//
//  Created by BC Holmes on 2018-08-22.
//  Copyright © 2018 Ayizan Studios. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "BCHSampSegment.h"

#import "BCHDataStream.h"

@interface BCHSampSegment()

@end


@implementation BCHSampSegment

-(NSDictionary*) samples {
    NSMutableDictionary* result = [NSMutableDictionary new];
    BCHDataStream* stream = [[BCHDataStream alloc] initWithData:self.data];
    
    while (![stream isEnded]) {
        uint32_t brushSize = [stream readBigEndianUInt32];
        NSLog(@"size %d", brushSize);
        NSInteger padding = 0;
        if ((brushSize % 4) != 0) {
            padding = 4 - (brushSize % 4);
        }
        
        NSString* key = [stream readAsciiStringOfLength:37 nullTerminated:NO];
        NSLog(@"key %@", key);
        if (self.subVersion == 1) {
            [stream readDataOfLength:10];
            brushSize -= 47;
        } else {
            [stream readDataOfLength:264];
            brushSize -= 301;
        }
        uint32_t top = [stream readBigEndianUInt32];
        uint32_t left = [stream readBigEndianUInt32];
        uint32_t bottom = [stream readBigEndianUInt32];
        uint32_t right = [stream readBigEndianUInt32];

        short depth = [stream readBigEndianUInt16];

        char compress = [stream readChar];
        
        NSLog(@"top = %d, left = %d, bottom = %d, right = %d, depth = %d, compress = %d, brushSize = %d, padding = %ld",
              top, left, bottom, right, depth, compress, brushSize, padding);
        NSLog(@"height = %d, width = %d", bottom - top, right - left);

        brushSize -= 19;
        
        NSData* imageData = [stream readDataOfLength:brushSize];
        if (padding) {
            [stream readDataOfLength:padding];
        }
        NSImage* image = [self createImage:imageData width:right-left height:bottom-top depth:depth compressed:compress];
        [result setObject:image forKey:key];
        
        //[self saveImage:image atPath:[[NSString stringWithFormat:@"~/Downloads/%@.png", [key substringFromIndex:1]] stringByExpandingTildeInPath]];
    }
    
    
    return [NSDictionary dictionaryWithDictionary:result];
}

-(void)saveImage:(NSImage*) image atPath:(NSString*) path {
    
    CGImageRef cgRef = [image CGImageForProposedRect:NULL
                                             context:nil
                                               hints:nil];
    NSBitmapImageRep *newRep = [[NSBitmapImageRep alloc] initWithCGImage:cgRef];
    [newRep setSize:[image size]];   // if you want the same resolution
    NSData *pngData = [newRep representationUsingType:NSPNGFileType properties:@{}];
    if (pngData == nil) {
        NSLog(@"png is nil");
    } else {
        NSLog(@"png length = %ld", pngData.length);
    }
    NSError* error;
    BOOL ok = [pngData writeToFile:path options:NSDataWritingAtomic error:&error];
    if (!ok) {
        NSLog(@"Nope. That didn't work: %@ %@", path, error);
    }
}

#define READ_SHORT(val) \
val = *(unsigned short *)brushData;\
val = CFSwapInt16BigToHost(val);\
brushData += sizeof(val);

-(NSImage*) createImage:(NSData*) data width:(long)w height:(long)h depth:(int)d compressed:(bool)compress
{
    int            *lineLengths;
    int            p, tw;
    unsigned short        len;
    
    unsigned char* brushData = (unsigned char*)[data bytes];
    long size = w * h * (d >> 3);
    NSBitmapImageRep* newBitmap = [[NSBitmapImageRep alloc]
                 initWithBitmapDataPlanes:0
                 pixelsWide:w pixelsHigh:h bitsPerSample:d
                 samplesPerPixel:1 hasAlpha:FALSE isPlanar:FALSE
                 colorSpaceName:NSDeviceBlackColorSpace
                 bytesPerRow:w bitsPerPixel:0];
    unsigned char* bitmapData = [newBitmap bitmapData];
    NSLog(@"bitmapData = %p", bitmapData);
    unsigned char* startBitmapData = bitmapData;
    int t = 0;
    if (compress) {
        // read compressed data
        lineLengths = malloc(h * sizeof(*lineLengths));
        for (int i = 0; i < h; i++) {
            READ_SHORT(len);
            lineLengths[i] = len;
            //BRLog(@"line[%d] = %d", i, lineLengths[i]);
        }
        for (int i = 0; i < h; i++) {
            tw = 0;
            //BRLog(@"i = %d", i);
            for (int j = 0; j < lineLengths[i];) {
                //BRLog(@"j = %d", j);
                //BRLog(@"delta = %d", bitmapData-startBitmapData);
                int n = *(char *)brushData++;
                ++j;
                // force p to be a signed number
                if (n >= 128) {
                    n -= 256;
                }
                if (n < 0) {
                    if (n == -128) {
                        continue; // nop
                    }
                    n = -n+1;
                    p = *(char *)brushData++;
                    j++;
                    for (int c = 0; c < n; c++) {
                        //BRLog(@"c = %d, n = %d", c, n);
                        *bitmapData++ = p;
                        ++t;
                        ++tw;
                    }
                    
                } else {
                    for (int c = 0; c < n+1; c++, j++) {
                        *bitmapData++ =
                        *(char *)brushData++;
                        ++t;
                        ++tw;
                    }
                }
            }
            if ((bitmapData - startBitmapData) > size) {
                NSLog(@"something funky is up with bitmapData");
            }
            if (tw != w) {
                NSLog(@"tw(%d) != width(%ld)",
                      tw, w);
            }
        }
        if (lineLengths) {
            free(lineLengths);
        }
    } else {
        NSLog(@"Brush not compressed!");
        memcpy(bitmapData, brushData, size);
    }
    NSImage* theImage = [[NSImage alloc] initWithSize:[newBitmap size]];
    [theImage addRepresentation:newBitmap];
    return theImage;
}

@end
