//
//  main.m
//  Brushy Brush
//
//  Created by BC Holmes on 2018-01-08.
//  Copyright © 2018 Ayizan Studios. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
