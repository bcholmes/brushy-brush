//
//  AppDelegate.m
//  Brushy Brush
//
//  Created by BC Holmes on 2018-01-08.
//  Copyright © 2018 Ayizan Studios. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
