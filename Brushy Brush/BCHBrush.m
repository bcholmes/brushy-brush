//
//  BCHBrush.m
//  Brushy Brush
//
//  Created by BC Holmes on 2018-08-25.
//  Copyright © 2018 Ayizan Studios. All rights reserved.
//

#import "BCHBrush.h"

#import <Cocoa/Cocoa.h>

@implementation BCHBrush

-(void)saveAsPng:(NSString*) path error:(NSError**) error {
    
    CGImageRef cgRef = [self.brushTipImage CGImageForProposedRect:NULL
                                             context:nil
                                               hints:nil];
    NSBitmapImageRep* newRep = [[NSBitmapImageRep alloc] initWithCGImage:cgRef];
    [newRep setSize:[self.brushTipImage size]];
    NSData *pngData = [newRep representationUsingType:NSPNGFileType properties:@{}];

    BOOL ok = [pngData writeToFile:path options:NSDataWritingAtomic error:error];
    if (!ok) {
        NSLog(@"Nope. That didn't work: %@ %@", path, *error);
    }
}
@end
