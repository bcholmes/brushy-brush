//
//  BCHDataStream.m
//  Brushy Brush
//
//  Created by BC Holmes on 2018-08-22.
//  Copyright © 2018 Ayizan Studios. All rights reserved.
//

#import "BCHDataStream.h"

@interface BCHDataStream()

@property (nonatomic, strong) NSData* data;
@property (nonatomic, assign) unsigned long currentOffset;

@end

@implementation BCHDataStream

-(id) initWithData:(NSData*) data {
    if (self = [super init]) {
        self.data = data;
        self.currentOffset = 0;
    }
    return self;
}

-(BOOL) isEnded {
    return self.currentOffset >= self.data.length;
}

-(NSData*) readDataOfLength:(NSUInteger) length {
    if (self.currentOffset < self.data.length) {
        NSLog(@"remaining size: %lu", self.data.length - self.currentOffset);
        NSData* result = [self.data subdataWithRange:NSMakeRange(self.currentOffset, MIN(length, self.data.length - self.currentOffset))];
        self.currentOffset += length;
        return result;
    } else {
        return nil;
    }
}

-(NSString*) readAsciiStringOfLength:(NSUInteger) length nullTerminated:(BOOL) nullTerminated {
    NSData* data = [self readDataOfLength:length];
    if (nullTerminated) {
        [self readDataOfLength:1];
    }
    if (data != nil) {
        return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
    } else {
        return nil;
    }
}

-(char) readChar {
    char* b = (char*) [self.data bytes];
    b += self.currentOffset;
    char temp = *(char *) b;
    self.currentOffset = self.currentOffset + (sizeof(temp));
    return temp;
}


-(uint16_t) readBigEndianUInt16 {
    char* b = (char*) [self.data bytes];
    b += self.currentOffset;
    uint16_t temp = *(uint16_t *) b;
    uint16_t result = CFSwapInt16BigToHost(temp);
    self.currentOffset = self.currentOffset + (sizeof(temp));
    return result;
}

-(uint32_t) readBigEndianUInt32 {
    char* b = (char*) [self.data bytes];
    b += self.currentOffset;
    uint32_t temp = *(uint32_t *) b;
    uint32_t result = CFSwapInt32BigToHost(temp);
    self.currentOffset = self.currentOffset + (sizeof(temp));
    return result;
}

@end
