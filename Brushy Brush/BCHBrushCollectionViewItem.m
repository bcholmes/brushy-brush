//
//  BCHBrushCollectionViewItem.m
//  Brushy Brush
//
//  Created by BC Holmes on 2018-08-25.
//  Copyright © 2018 Ayizan Studios. All rights reserved.
//

#import "BCHBrushCollectionViewItem.h"

@interface BCHBrushCollectionViewItem ()

@end

@implementation BCHBrushCollectionViewItem

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
}

-(void)rightMouseDown:(NSEvent *)theEvent {
    
    NSMenu *theMenu = [[NSMenu alloc] initWithTitle:@"Brush context menu"];
    [theMenu insertItemWithTitle:@"Save image as..."
                          action:@selector(export:)
                   keyEquivalent:@""
                         atIndex:0];
    
    [NSMenu popUpContextMenu:theMenu withEvent:theEvent forView:self.view];
    
    [super rightMouseDown:theEvent];
    
}


-(void) export:(id) sender {
    NSSavePanel *panel = [NSSavePanel savePanel];
    
    [panel setMessage:@"Please select a location to save your brush image."];
    [panel setAllowsOtherFileTypes:YES];
    [panel setExtensionHidden:NO];
    [panel setCanCreateDirectories:YES];
    [panel setNameFieldStringValue:[NSString stringWithFormat:@"%@.png", self.brush.brushTipImageKey]];
    [panel setTitle:@"Saving checkboard..."];
    
    NSInteger result = [panel runModal];
    NSError *error = nil;
    
    if (result == NSModalResponseOK) {
        NSString *path0 = [[panel URL] path];
        [self.brush saveAsPng:path0 error:&error] ;
        
        if (error) {
            [NSApp presentError:error];
        }
    }
}
@end

