//
//  BCHBrushCollectionViewItem.h
//  Brushy Brush
//
//  Created by BC Holmes on 2018-08-25.
//  Copyright © 2018 Ayizan Studios. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "BCHBrush.h"

@interface BCHBrushCollectionViewItem : NSCollectionViewItem

@property (nonatomic, weak) IBOutlet NSImageView* brushImageView;

@property (nonatomic, strong) BCHBrush* brush;

@end
