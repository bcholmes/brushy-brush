//
//  BCHDataStream.h
//  Brushy Brush
//
//  Created by BC Holmes on 2018-08-22.
//  Copyright © 2018 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BCHDataStream : NSObject

-(id) initWithData:(NSData*) data;
-(uint16_t) readBigEndianUInt16;
-(uint32_t) readBigEndianUInt32;
-(NSData*) readDataOfLength:(NSUInteger) length;
-(BOOL) isEnded;
-(NSString*) readAsciiStringOfLength:(NSUInteger) length nullTerminated:(BOOL) nullTerminated;
-(char) readChar;

@end
