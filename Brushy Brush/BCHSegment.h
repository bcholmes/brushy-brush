//
//  BCHSegment.h
//  Brushy Brush
//
//  Created by BC Holmes on 2018-08-22.
//  Copyright © 2018 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BCHSegment : NSObject

@property (nonatomic, strong) NSString* tagName;
@property (nonatomic, assign) unsigned long length;
@property (nonatomic, strong) NSData* data;

@end
