//
//  ViewController.m
//  Brushy Brush
//
//  Created by BC Holmes on 2018-01-08.
//  Copyright © 2018 Ayizan Studios. All rights reserved.
//

#import "BCHBrushViewController.h"

#import "BCHBrush.h"
#import "BCHBrushDocument.h"
#import "BCHBrushCollectionViewItem.h"

@interface BCHBrushViewController()<NSCollectionViewDataSource,NSCollectionViewDelegate>

@property (nonatomic, weak) IBOutlet NSTextField* versionLabel;
@property (nonatomic, weak) IBOutlet NSCollectionView* collectionView;

@property (nonatomic, readonly) BCHBrushDocument* brushDocument;
@property (nonatomic, readonly) AbrHeader* abrHeader;

@end

@implementation BCHBrushViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView registerClass:[BCHBrushCollectionViewItem class] forItemWithIdentifier:@"BrushCell"];
}

-(void) viewWillAppear {
    [super viewWillAppear];
    NSLog(@"viewWillAppear");
    [self populateViews];
}

-(AbrHeader*) abrHeader {
    return self.brushDocument.header;
}

-(BCHBrushDocument*) brushDocument {
    return (BCHBrushDocument*) self.view.window.windowController.document;
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];
    
    NSLog(@"setRepresentedObject");

    // Update the view, if already loaded.
    [self populateViews];
}

-(void) populateViews {
    if (self.brushDocument != nil) {
        NSLog(@"document: %@", self.brushDocument == nil ? @"null" : @"not null");
        NSLog(@"document header: %@", self.abrHeader == nil ? @"null" : @"not null");
        NSLog(@"populate");
        if (self.abrHeader != nil) {
            [self.versionLabel setStringValue:self.abrHeader.fullVersion];
        }
        [self.collectionView reloadData];
    }
}

-(NSInteger) collectionView:(NSCollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSLog(@"number of brushes: %ld", self.brushDocument.brushes.count);
    return self.brushDocument.brushes.count;
}

-(NSCollectionViewItem*) collectionView:(NSCollectionView*) collectionView itemForRepresentedObjectAtIndexPath:(NSIndexPath*) indexPath {
    BCHBrushCollectionViewItem* item = [self.collectionView makeItemWithIdentifier:@"BrushCell" forIndexPath:indexPath];
    BCHBrush* brush = [self.brushDocument.brushes objectAtIndex:indexPath.item];
    item.brushImageView.image = brush.brushTipImage;
    item.brush = brush;
    return item;
}

@end
