//
//  BCHBrush.h
//  Brushy Brush
//
//  Created by BC Holmes on 2018-08-25.
//  Copyright © 2018 Ayizan Studios. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BCHBrush : NSObject

@property (nonatomic, strong) NSImage* brushTipImage;
@property (nonatomic, strong) NSString* brushTipImageKey;

-(void)saveAsPng:(NSString*) path error:(NSError**) error;

@end
